function curry(fn,arity = fn.length) {
    return (function nextCurried(prevArgs){
        return function curried(nextArg){
            console.log('func: ', fn.toString());
            console.log('fnc length: ', arity)
            console.log('preArgs: ', prevArgs)
            console.log('nextArg: ', nextArg)
            var args = [ ...prevArgs, nextArg ];
            console.log('args.length ', args.length)
            if (args.length >= arity) {
                return fn( ...args );
            }
            else {
                return nextCurried( args );
            }
        };
    })( [] );
}

const pipe = function(...fns) {
    return function(x) {
        console.log('x: ', x)
        return fns.reduce(function(v, f) {
            console.log('reduce function: ', f.toString())
            console.log('value: ', v)
            return f(v);
        }, x);
    }
};

const compose = function(...fns) {
    return function(x) {
        return fns.reduceRight(function(v, f) {
            return f(v);
        }, x);
    }
};
